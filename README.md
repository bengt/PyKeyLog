# PyKeyLog

PyKeyLog is an unconcealed Keylogger for Linux written in Python. It wraps
around [python-evdev](https://github.com/gvalkov/python-evdev).

It logs keystrokes to a file and makes no attempt to hide itself. If you need a
keylogger to stay hidden for capturing intruders actions of your honey pot, use
something else.

## Dependencies

-   python-evdev
-   kernel-headers
-   python-devel

## Installing Non-Python Dependencies

On Fedora:

    sudo yum install kernel-headers python-devel

On Debian/Ubuntu it should be something like:

    sudo apt-get install kernel-headers python-dev

## Installing Python Dependencies

    sudo pip install -U -r requirements.txt

Fedora an extra-special snowflake and thinks, pip needs to be suffixed, so:

    sudo pip-python install -U -r requirements.txt

## Running

PyKeyLog records all input events decoded by a german keymap to a file in the
present directory.

    sudo python PyKeyLog.py

## Known Issues

-   Records almost every keystroke twice.
